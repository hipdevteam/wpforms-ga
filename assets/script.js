document.addEventListener('DOMContentLoaded', function() {
  let service_el = document.getElementById('wpforms-ga-service');

  let ua_options = document.getElementById('ua_options');
  let ga4_options = document.getElementById('ga4_options');

  ga4_options.style.display = 'none';
  ua_options.style.display = 'none';

  if ( service_el.value === 'ga4' ) {
    ga4_options.style.display = 'block';
  }

  if ( service_el.value === 'ua' ) {
    ua_options.style.display = 'block';
  }

  service_el.addEventListener('change', function(event) {
    var value = event.target.value;

    ga4_options.style.display = 'none';
    ua_options.style.display = 'none';
    if ( value === 'ga4' ) {
      ga4_options.style.display = 'block';
    }
    if ( value === 'ua' ) {
      ua_options.style.display = 'block';
    }
  });
});
