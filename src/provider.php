<?php
use TheIconic\Tracking\GoogleAnalytics\Analytics;
use Br33f\Ga4\MeasurementProtocol\Service;
use Br33f\Ga4\MeasurementProtocol\Dto\Request\BaseRequest;
use Br33f\Ga4\MeasurementProtocol\Dto\Event\BaseEvent;

class GA_WPForms extends WPForms_Provider
{
    public function init()
    {
        $this->version = GA_WPFORMS_VERSION;
        $this->name = "Google Analytics";
        $this->slug = "google-analytics";
        $this->priority = 30;
        $this->icon = GA_WPFORMS_URL . '/assets/icon.jpg';
    }

    public function api_auth($data = array(), $form_id = '')
    {
        $id = uniqid();
        $providers = get_option('wpforms_providers', []);

        if ( $data['service'] == 'ga4' ) {
            $providers[$this->slug][$id] = [
                'service'        => 'ga4',
                'measurement_id' => trim($data['measurement_id']),
                'api_secret'     => trim($data['api_secret']),
                'label'          => sanitize_text_field($data['label']),
                'date'           => time()
            ];
        } elseif ($data['service'] == 'ua') {
            $providers[$this->slug][$id] = [
                'service'   => 'ua',
                'trackid'   => trim($data['trackid']),
                'label'     => sanitize_text_field($data['label']),
                'date'      => time()
            ];
        } else {
            return new WP_Error('wpforms_ga', "Missing required Parameters.");
        }

        update_option('wpforms_providers', $providers);

        return $id;
    }

    public function process_entry($fields, $entry, $form_data, $entry_id = 0)
    {
        $providers = get_option('wpforms_providers');

        $ip = $_SERVER['REMOTE_ADDR'];
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $label = $form_data['settings']['form_title'];
        $uid = md5($ip);

        foreach ($providers[$this->slug] as $connection) {
            if ( $connection['service'] == "ga4" ) {
                $service = new Service($connection['api_secret'],
                                       $connection['measurement_id']);

                $request = new BaseRequest($uid);

                $eventData = new BaseEvent("form_submission");
                $eventData->setForm($label);

                $request->addEvent($eventData);
                $service->send($request);
            } else {
                $analytics = new Analytics();
                $analytics->setProtocolVersion('1')
                          ->setClientId(md5($ip))
                          ->setEventCategory('Form')
                          ->setEventAction('Submit')
                          ->setEventLabel($label);

                $analytics->setTrackingId($connection['trackid']);
                $response = $analytics->sendEvent();
            }
        }
    }

    public function integrations_tab_new_form()
    {
        ?>
            <select name="service" id="wpforms-ga-service">
                <option value="">---- Please Select Service ----</option>
                <option value="ga4">Google Analytics 4</option>
                <option value="ua">Universal Analytics (Deprecated)</option>
            </select>

            <span id="ua_options">
              <input type="text" name="trackid" placeholder="Universal Analytics ID">
              <input type="text" name="label" placeholder="Account Nickname">
            </span>

            <span id="ga4_options">
              <input type="text" name="measurement_id" placeholder="GA4 Measurement ID">
              <input type="password" name="api_secret" placeholder="GA4 API Secret">
              <input type="text" name="label" placeholder="Account Nickname">
            </span>
        <?php
    }

    public function builder_output()
    {
        ?>
        <div class="wpforms-panel-content-section wpforms-panel-content-section-<?php echo $this->slug; ?>" id="<?php echo $this->slug; ?>-provider">

            <?php $this->builder_output_before(); ?>

            <div class="wpforms-panel-content-section-title">

                <?php echo $this->name; ?>

            </div>

            <div class="wpforms-provider-connections-wrap wpforms-clear">

                <div class="wpforms-provider-connections">

                    <h2>You are all Good!</h2>
                    <p>There is nothing for you to configure here.</p>

                </div>

            </div>

            <?php $this->builder_output_after(); ?>

        </div>
        <?php
    }
}
